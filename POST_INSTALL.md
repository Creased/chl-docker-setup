# Configuration de Docker-CE sur Debian Jessie

## Création d'un utilisateur de service

Par défaut, le daemon docker est en écoute sur un socket UNIX, dont le propriétaire et la gestion dépend de l'utilisateur root, par conséquent, seules les utilisateurs disposant d'un accès à un shell intéractif dans le contexte d'exécution de root peuvent y accèder.

Afin de passer outre cette limitation, le daemon docker supporte l'utilisation d'un groupe UNIX appelé `docker`.

Création du groupe `docker`&nbsp;:

```bash
groupadd docker
```

Création d'un utilisateur standard (si ça n'est pas déjà le cas)&nbsp;:

```bash
useradd --system --uid 1337 --user-group --groups users \
        --home-dir /home/user/ --shell /bin/bash \
        --comment "Utilisateur standard du système" user
mkdir /home/user/
find /etc/skel/ -mindepth 1 -exec cp -r {} /home/user/ \;
chown -R user:user /home/user/
chmod -R 700 /home/user/
passwd user
```

Ajout de l’utilisateur aux membres du groupe docker&nbsp;:

```bash
usermod -aG docker user
```

## Configuration du proxy pour Docker

```bash
mkdir -p /etc/systemd/system/docker.service.d
cat <<-'EOF' >/etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://172.18.4.1:8080/"
Environment="HTTPS_PROXY=http://172.18.4.1:8080/"
Environment="NO_PROXY=localhost,127.0.0.0/8,*.miletrie.chl"

EOF
systemctl daemon-reload
systemctl show --property Environment docker.service
systemctl restart docker.service docker.socket
```

## Configuration de Docker pour démarrer automatiquement

```bash
systemctl enable docker
```

## Configuration du service

```bash
cat <<-'EOF' >/etc/default/docker
##
# Location of Docker binary (especially for development testing)
#
DOCKERD="/usr/bin/dockerd"

##
# General daemon startup options
# https://docs.docker.com/engine/reference/commandline/docker/
#
DOCKER_OPTS="--ip=0.0.0.0 \
             --ip-masq=false \
             --ip-forward=false \
             --iptables=false \
             --userland-proxy=false \
             --icc=true \
             --bip=172.16.57.238/28 \
             --fixed-cidr=172.16.57.224/28 \
             --dns=172.16.10.32 \
             --dns=172.16.10.33 \
             --dns-search=miletrie.chl"

EOF
cat <<-'EOF' >/etc/systemd/system/docker.service.d/exec.conf
[Service]
EnvironmentFile=-/etc/default/docker
ExecStart=
ExecStart=/usr/bin/dockerd $DOCKER_OPTS

EOF
systemctl daemon-reload
systemctl restart docker.service docker.socket
```
