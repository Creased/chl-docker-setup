# Mise en place d'un environnement de développement WEB sous Docker-CE

## Installation de Docker-Compose

```bash
curl --location --url "https://github.com/docker/compose/releases/download/$(curl --head --silent --url 'https://github.com/docker/compose/releases/latest' | grep -oP '^(?:Location).+$' | awk -F'/' '{print $(NF)}' | grep -oP '([0-9]\.?){2,}')/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose version
```

## Téléchargement du projet

```bash
cd /opt
git clone https://gitlab.com/Creased/docker-webdev-env
pushd docker-webdev-env/
```

## Configuration du proxy (étape optionnelle)

```bash
cat <<-'EOF' | tee build/{php-fpm,stackedit}/70debconf
Acquire::http::Proxy "http://172.18.4.1:8080/";
Acquire::https::Proxy "https://172.18.4.1:8080/";
Acquire::socks::Proxy "socks://172.18.4.1:8080/";

EOF
perl -p -i -e 's@^#?\s*(COPY .+/70debconf.+)$@$1@' ./build/*/Dockerfile
```

## Composition des images

```bash
docker-compose pull
docker-compose build
```

## Récupération de la configuration IP des conteneurs

```bash
docker inspect front
```

Exemple&nbsp;:

```json
[
    {
        "Name": "front",
        "Id": "dd84d160afbe9c3765dc4c4103837d7c9a3bc64c1679aaefc82928cdf407cbe6",
        "Created": "2017-03-17T14:35:40.818824539+01:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.16.0.0/16",
                    "IPRange": "172.16.57.224/28",
                    "Gateway": "172.16.57.148",
                    "AuxiliaryAddresses": {
                        "DefaultGatewayIPv4": "172.16.10.1"
                    }
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Containers": {
            "854d31eeab8074230e7bc80e4946b562eccf17f281f7e7278277fc20c4e5239e": {
                "Name": "http",
                "EndpointID": "61cab4e98826fa95ebe6ca11223d43366d821abb87bcf44385c348c2d3ae793d",
                "MacAddress": "02:42:ac:10:39:e0",
                "IPv4Address": "172.16.57.224/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "false",
            "com.docker.network.bridge.name": "br0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
```

<u>Note&nbsp;:</u> On admet que les conteneurs sont effectivement connectés au pont (option `network: front` dans [docker-compose.yml](docker-compose.yml)).

## Exemple pour l'ajout d'un projet de test

```bash
pushd data/www/lab/
git clone https://gitlab.com/Creased/relative-path-overwrite
popd
```

Le projet est dorénavant accessible depuis [local.dev/relative-path-overwrite](http://local.dev/relative-path-overwrite/).
