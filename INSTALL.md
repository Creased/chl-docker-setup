# Installation de Docker-CE sur Debian Jessie

## Suppression des anciennes versions

Depuis la [release 17.03](https://github.com/docker/docker.github.io/pull/2050), le nom du package contenant le moteur Docker-Engine s'appelle `docker-ce`, avant de procéder à son installation, il faut donc procéder à la suppression des anciens paquets&nbsp;:

```bash
apt-get remove docker docker-engine
```

<u>Note :</u> Le contenu de `/var/lib/docker/`, comprenant les images, conteneurs, volumes et réseaux est préservé lors de la suppression du paquet.

## Configuration des dépôts APT

Pour installer Docker en utilisant le gestionnaire de paquets APT, il est nécessaire de configurer un nouveau dépôt dans les sources de APT. Cependant, afin d'exploiter ce nouveau dépôt sur HTTPS, il est nécessaire d'installer quelques outils&nbsp;:

```bash
apt-get install curl ca-certificates apt-transport-https software-properties-common
```

Téléchargement de la clé GPG de Docker&nbsp;:

```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
```

Ajout dépôt&nbsp;:

```bash
cat <<-'EOF' >/etc/apt/sources.list.d/docker.list
deb [arch=amd64] https://download.docker.com/linux/debian jessie stable

EOF
```

Mise à niveau des caches&nbsp;:

```bash
apt-get clean
apt-get update
```

## Installation de Docker-CE

```bash
apt-get install docker-ce
```
