# Création d'un pont pour Docker-CE

## Ajout de fonctions au contexte

Ajout de fonctions pour la configuration de DockerD&nbsp;:

```bash
cat <<-'EOF' >>~/.bash_profile
cdr2mask () {
    ## https://forums.gentoo.org/viewtopic-t-888736-start-0.html
    # Number of args to shift, 255..255, first non-255 byte, zeroes
    set -- $(( 5 - ($1 / 8) )) 255 255 255 255 $(( (255 << (8 - ($1 % 8))) & 255 )) 0 0 0
    [ $1 -gt 1 ] && shift $1 || shift
    echo ${1-0}.${2-0}.${3-0}.${4-0}
}

#======================================#
# [+] Title:   DockerD Options Editor  #
# [+] Author:  Baptiste M. (Creased)   #
# [+] Website: bmoine.fr               #
# [+] Email:   contact@bmoine.fr       #
# [+] Twitter: @Creased_               #
#======================================#

dockerd-del-opt() {
    ###
    # Variables
    #
    OPTIONS=${1:-''}
    FILE=/etc/default/docker

    ###
    # Main process
    #
    if [ ! -z "${OPTIONS}" ]; then
        for OPT in $OPTIONS; do (
            # Display the ongoing operation
            printf "\033[0;33m[-]\033[0m \033[1;36m%s\033[0m\n" "${OPT}"

            # Escape option in order to prevent sed to treat special chars
            OPT=$(echo "${OPT}" | sed -e 's/[]\/$*.^|[]/\\&/g')

            sed -i -e '/DOCKER_OPTS="/,/"/{
                :a;N;$!ba;                                              # Add all lines to buffer
                s/^\(DOCKER_OPTS="\)\([^\n\r]*\)/\1\n             \2/;  # Handle empty DOCKER_OPTS by adding a new temporary line
                :remove;{                                               # Remove option within DOCKER_OPTS
                    s/\([ \\]\)[\r\n]*[^\r\n]*--'${OPT}'\(\(=[^="\r\n]*\)\|\([^=a-zA-Z0-9"\r\n]*\)\)\(["\r\n]\)/\1\5/;
                    s/ \\\("\n$\)/\1/
                }; t remove;
                s/^\(DOCKER_OPTS="\)[\n ]*\([^\n\r]*\)/\1\2/            # Remove temporary line
            }' ${FILE}
        ); done
        printf "\n"
    fi

    ###
    # Display changes
    #
    cat ${FILE}
}

dockerd-add-opt() {
    ###
    # Variables
    #
    OPTIONS=${1:-''}
    FILE=/etc/default/docker

    ###
    # Main process
    #
    if [ ! -z "${OPTIONS}" ]; then
        # Prevent duplicate by removing existant option
        dockerd-del-opt "${OPTIONS}"

        for OPT in $OPTIONS; do (
            # Display the ongoing operation
            printf "\033[0;33m[+]\033[0m \033[1;36m%s\033[0m\n" "${OPT}"

            # Escape option in order to prevent sed to treat special chars
            OPT=$(echo "${OPT}" | sed -e 's/[]\/$*.^|[]/\\&/g')

            sed -i -e '/DOCKER_OPTS="/,/"/{
                :a;N;$!ba;                                                        # Add all lines to buffer
                s/^\(DOCKER_OPTS="[^"][^"]*\)"/\1 \\\n             --'${OPT}'"/;  # Add option within DOCKER_OPTS
                s/^\(DOCKER_OPTS="\)"/\1--'${OPT}'"/                              # Add option within empty DOCKER_OPTS
            }' ${FILE}
        ); done
        printf "\n"
    fi

    ###
    # Display changes
    #
    cat ${FILE}
}

EOF
source ~/.bash_profile
```

## Définition des variables contextuelles

```bash
export NIC_FRONT=eth0
export NIC_NAT=eth1
export BR_FRONT=br0
export BR_BACK=br1
export BR_NAT=br2

export IP_FRONT_SUBNET=172.16.0.0/16
export IP_FRONT_RANGE=172.16.57.224/28
export IP_FRONT_CIDR=$(ip addr show ${NIC_FRONT} | grep -oE 'inet[ ]+[^ ]+' | awk -F' ' '{print $2}')
export IP_FRONT=$(printf "%s" "${IP_FRONT_CIDR}" | awk -F'/' '{print $1}')
export IP_FRONT_MASK=$(cdr2mask $(printf "%s" "${IP_FRONT_CIDR}" | awk -F'/' '{print $2}'))
export IP_FRONT_GW=$(ip route show | grep -oE 'default via [^ ]+' | awk -F' ' '{print $3}' | tail -n1)
export IP_FRONT_DNS1=172.16.10.32
export IP_FRONT_DNS2=172.16.10.33
export IP_FRONT_DOMAIN=miletrie.chl

export IP_BACK_SUBNET=10.1.0.0/16
export IP_BACK_RANGE=10.1.0.0/24
export IP_BACK_CIDR=10.1.0.254/24
export IP_BACK=$(printf "%s" "${IP_BACK_CIDR}" | awk -F'/' '{print $1}')
export IP_BACK_MASK=$(cdr2mask $(printf "%s" "${IP_BACK_CIDR}" | awk -F'/' '{print $2}'))

export IP_NIC_NAT_SUBNET=${IP_FRONT_SUBNET}
export IP_NIC_NAT_CIDR=172.16.57.149/16
export IP_NIC_NAT=$(printf "%s" "${IP_NIC_NAT_CIDR}" | awk -F'/' '{print $1}')
export IP_NIC_NAT_MASK=$(cdr2mask $(printf "%s" "${IP_NIC_NAT_CIDR}" | awk -F'/' '{print $2}'))
export IP_NIC_NAT_GW=${IP_FRONT_GW}
export IP_NIC_NAT_DNS1=${IP_FRONT_DNS1}
export IP_NIC_NAT_DNS2=${IP_FRONT_DNS2}
export IP_NIC_NAT_DOMAIN=${IP_FRONT_DOMAIN}

export IP_NAT_CIDR=10.0.0.254/16
export IP_NAT=$(printf "%s" "${IP_NAT_CIDR}" | awk -F'/' '{print $1}')
export IP_NAT_MASK=$(cdr2mask $(printf "%s" "${IP_NAT_CIDR}" | awk -F'/' '{print $2}'))
```

Debug&nbsp;:

```bash
VAR="NIC_FRONT NIC_NAT BR_FRONT BR_BACK BR_NAT IP_FRONT_SUBNET IP_FRONT_RANGE IP_FRONT_CIDR IP_FRONT IP_FRONT_MASK IP_FRONT_GW IP_FRONT_DNS1 IP_FRONT_DNS2 IP_FRONT_DOMAIN IP_BACK_SUBNET IP_BACK_RANGE IP_BACK_CIDR IP_BACK IP_BACK_MASK IP_NIC_NAT_SUBNET IP_NIC_NAT_CIDR IP_NIC_NAT IP_NIC_NAT_MASK IP_NIC_NAT_GW IP_NIC_NAT_DNS1 IP_NIC_NAT_DNS2 IP_NIC_NAT_DOMAIN IP_NAT_CIDR IP_NAT IP_NAT_MASK"
for V in ${VAR}; do (
    printf "%-20s => %s\n" "${V}" "${!V}"
); done
```

## Fermeture des services Docker

```bash
systemctl stop docker.service docker.socket
```

## Suppression du pont par défaut

```bash
ip link set dev docker0 down
brctl delbr docker0
```

## Création temporaire des ponts

### Création des ponts

Création des ponts&nbsp;:

```bash
brctl addbr ${BR_FRONT}
brctl addbr ${BR_BACK}
```

Montage des interfaces virtuelles&nbsp;:

```bash
ip link set dev ${BR_FRONT} up
ip link set dev ${BR_BACK} up
```

### Configuration IP des ponts

Ajout du contrôleur d'interface réseau physique au pont&nbsp;:

```bash
brctl addif ${BR_FRONT} ${NIC_FRONT}
```

Déplacement de la configuration IP du contrôleur d'interface réseau physique au pont&nbsp;:

```bash
ip addr del ${IP_FRONT_CIDR} dev ${NIC_FRONT}
ip addr add ${IP_FRONT_CIDR} dev ${BR_FRONT}
```

Configuration IP du pont Backend&nbsp;:

```bash
ip addr add ${IP_BACK_CIDR} dev ${BR_BACK}
```

Mise à jour de la route par défaut&nbsp;:

```bash
ip route del default
ip route add default via ${IP_FRONT_GW} dev ${BR_FRONT}
```

Création d'un nouveau réseau de Frontend dans Docker&nbsp;:

```bash
dockerd-del-opt "bip fixed-cidr dns bridge ip ip-forward userland-proxy ip-masq iptables icc"
dockerd-add-opt "bridge=${BR_NAT} ip=${IP_NIC_NAT} ip-forward=true userland-proxy=true ip-masq=true iptables=false icc=true"
systemctl daemon-reload
systemctl restart docker.service docker.socket
docker network create \
       --driver bridge \
       --subnet=${IP_FRONT_SUBNET} \
       --ip-range=${IP_FRONT_RANGE} \
       --gateway=${IP_FRONT} \
       --aux-address "DefaultGatewayIPv4=${IP_FRONT_GW}" \
       --opt "com.docker.network.bridge.default_bridge"="true" \
       --opt "com.docker.network.bridge.enable_icc"="true" \
       --opt "com.docker.network.bridge.enable_ip_masquerade"="false" \
       --opt "com.docker.network.bridge.name"="${BR_FRONT}" \
       --opt "com.docker.network.driver.mtu"="1500" \
       front
```

Création d'un nouveau réseau de Backend dans Docker&nbsp;:

```bash
docker network create \
       --driver bridge \
       --subnet=${IP_BACK_SUBNET} \
       --ip-range=${IP_BACK_RANGE} \
       --gateway=${IP_BACK} \
       --opt "com.docker.network.bridge.default_bridge"="false" \
       --opt "com.docker.network.bridge.enable_icc"="true" \
       --opt "com.docker.network.bridge.enable_ip_masquerade"="false" \
       --opt "com.docker.network.bridge.enable_internal"="true" \
       --opt "com.docker.network.bridge.name"="${BR_BACK}" \
       --opt "com.docker.network.driver.mtu"="1500" \
       back
```

## Configuration du NAT

```bash
cat <<-EOF >/etc/network/iptables.sh
#!/usr/bin/env bash

#=======================================#
# [+] Title:   IPTables Rules Generator #
# [+] Author:  Baptiste M. (Creased)    #
# [+] Website: bmoine.fr                #
# [+] Email:   contact@bmoine.fr        #
# [+] Twitter: @Creased_                #
#=======================================#

##
# Configuration du script
#
## Création d'un nouveau flux de sortie
exec 3>/dev/stdout

## Flux de sortie standard (1>) et flux de sortie d'erreur (2>) vers le blackhole
exec &>/dev/null

##
# Configurations de base
#
iptables-save >/etc/network/iptables.backup # Sauvegarde la configuration actuelle
iptables -F                                 # Vide (flush) la table de configuration du pare-feu actuellement utilisée
iptables -X                                 # Supprime les tables qui ne sont pas par défaut
iptables -t nat -F                          # Vide les règles nat, relatives au rôle de passerelle (non installé ...)
iptables -t mangle -F                       # Vide les règles mangle, relatives à la QoS
iptables -P INPUT ACCEPT                    # Autorise par défaut toute les requêtes entrantes
iptables -A INPUT -i lo -j ACCEPT           # Autorise par défaut toute les requêtes entrantes de la carte de bouclage (loopback)
iptables -P OUTPUT ACCEPT                   # Autorise par défaut toute les requêtes sortantes
iptables -P FORWARD ACCEPT                  # Autorise par défaut toute les requêtes passant d'une carte réseau à une autre
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT  # Autorise par défaut tous les paquets faisant partie d'une flux TCP établie (donc autorisé)

##
# Fonctions
#
cdr2mask () {
    ## https://forums.gentoo.org/viewtopic-t-888736-start-0.html
    # Number of args to shift, 255..255, first non-255 byte, zeroes
    set -- \$(( 5 - (\$1 / 8) )) 255 255 255 255 \$(( (255 << (8 - (\$1 % 8))) & 255 )) 0 0 0
    [ \$1 -gt 1 ] && shift \$1 || shift
    echo \${1-0}.\${2-0}.\${3-0}.${4-0}
}

##
# Variables
#
NIC_NAT=${NIC_NAT}
BR_FRONT=${BR_FRONT}
BR_NAT=${BR_NAT}

IP_NIC_NAT_CIDR=\$(ip addr show \${NIC_NAT} | grep 'inet' | head -n1 | awk '{print \$2}')
IP_NIC_NAT_SUBNET=\$(ip route show | grep \${NIC_NAT} | awk '{print \$1}' | tail -1)

IP_FRONT_GW=\$(ip route show table main | grep -oE "default via [^ ]+ dev \${BR_FRONT}" | awk -F' ' '{print \$3}' | tail -n1)
IP_BR_NAT_SUBNET=\$(ip route show | grep \${BR_NAT} | awk '{print \$1}' | tail -1)

VAR="NIC_NAT BR_FRONT BR_NAT IP_NIC_NAT_CIDR IP_NIC_NAT_SUBNET IP_FRONT_GW IP_BR_NAT_SUBNET"
for V in \${VAR}; do (
    printf "%-20s => %s\n" "\${V}" "\${!V}" >&3
); done

##
# Routeur
#
## Activation du routage au niveau noyau
sysctl net.ipv4.ip_forward=1
sysctl net.ipv4.conf.all.forwarding=1
sysctl net.ipv4.conf.default.forwarding=1
sysctl -p  # Recharge la nouvelle configuration
modprobe iptable_nat  # Chargement du module NAT

## Création d'une table de routage pour Docker
perl -p -i -e 's/^.+[\t ]+docker\n\$//' /etc/iproute2/rt_tables
echo "1001 docker" >>/etc/iproute2/rt_tables
ip route del default
ip route del default table docker
ip route add default via \${IP_FRONT_GW} dev \${BR_FRONT} priority 0
ip route add default via \${IP_FRONT_GW} dev \${NIC_NAT} priority 1
ip route add default via \${IP_FRONT_GW} dev \${NIC_NAT} priority 0 table docker
ip route add \${IP_NIC_NAT_SUBNET} dev \${NIC_NAT} priority 0 table docker

ip rule del from all
ip rule flush
ip rule add priority 0 from \${IP_BR_NAT_SUBNET} fwmark 0x1 lookup docker  # Le trafic en provenance du réseau NATé et marqué 1 sera traité par la table de routage docker
ip rule add from all lookup local priority 1
ip rule add from all lookup main priority 32766
ip rule add from all lookup default priority 32767

## Configuration de l'IP Masquerading
# 1. Initial Request: equest: DOCKER_CONTAINER (ICMP) (internal_nic->\${BR_NAT}) -> WAN_IP
# 2. Marking: DOCKER_CONTAINER (ICMP) + MARK 0x1 (\${BR_NAT}) -> WAN_IP
# 3. Forwarding + Routing: DOCKER_CONTAINER (ICMP) + MARK 0x1 (\${BR_NAT}->\${NIC_NAT}) -> WAN_IP
# 4. Sending: DOCKER_CONTAINER (ICMP) + MARK 0x1 (\${NIC_NAT}) -> WAN_IP

iptables -t mangle -A PREROUTING -i \${BR_NAT} -j MARK --set-mark 0x1
iptables -t nat -A POSTROUTING -s \${IP_BR_NAT_SUBNET} -o \${NIC_NAT} -j MASQUERADE
iptables -A FORWARD -i \${BR_NAT} ! -o \${NIC_NAT} -j DROP

## Vidage du cache des routes
ip route flush cache

##
# Sauvegarde de la configuration
#
iptables-save >/etc/network/iptables.up.rules
cat /etc/network/iptables.up.rules >&3

EOF
chmod 755 /etc/network/iptables.sh
```

## Configuration persistante des ponts

```bash
cat <<-EOF >/etc/network/interfaces
##
# Default configuration
#
iface default inet dhcp

##
# Loopback NIC configuration
#
auto lo
iface lo inet loopback

##
# Ethernet NIC configuration
#
auto ${NIC_FRONT}
allow-hotplug ${NIC_FRONT}
iface ${NIC_FRONT} inet manual

auto ${NIC_NAT}
allow-hotplug ${NIC_NAT}
iface ${NIC_NAT} inet static
    address ${IP_NIC_NAT}
    netmask ${IP_NIC_NAT_MASK}
    gateway ${IP_NIC_NAT_GW}
    dns-search ${IP_NIC_NAT_DOMAIN}
    dns-nameservers ${IP_NIC_NAT_DNS1} ${IP_NIC_NAT_DNS2}

##
# Bridges configuration
#
auto ${BR_FRONT}
iface ${BR_FRONT} inet static
    bridge_ports ${NIC_FRONT}
    address ${IP_FRONT}
    netmask ${IP_FRONT_MASK}
    gateway ${IP_FRONT_GW}
    dns-search ${IP_FRONT_DOMAIN}
    dns-nameservers ${IP_FRONT_DNS1} ${IP_FRONT_DNS2}

auto ${BR_BACK}
iface ${BR_BACK} inet static
    bridge_ports none
    address ${IP_BACK}
    netmask ${IP_BACK_MASK}

auto ${BR_NAT}
iface ${BR_NAT} inet static
    bridge_ports none
    address ${IP_NAT}
    netmask ${IP_NAT_MASK}
    post-up /etc/network/iptables.sh

EOF
ifdown ${NIC_FRONT} && ifup ${NIC_FRONT}
ifdown ${NIC_NAT} && ifup ${NIC_NAT}
ifdown ${BR_FRONT} && ifup ${BR_FRONT}
ifdown ${BR_BACK} && ifup ${BR_BACK}
ifdown ${BR_NAT} && ifup ${BR_NAT}
```

Redémarrage des services Docker&nbsp;:

```bash
systemctl restart docker.service docker.socket
```

Test de communication avec l'extérieur depuis les interfaces de l'hôte et depuis un conteneur&nbsp;:

```bash
ping -c5 -I ${BR_FRONT} ${IP_FRONT_GW}
ping -c5 -I ${NIC_NAT} ${IP_FRONT_GW}
docker kill nat_test; docker run --rm --net bridge --name nat_test busybox ping -c5 ${IP_FRONT_GW}
```
